# Projeto Kawabe WebApp:

[Trello](https://trello.com/b/tqXlBRze/kawabe), [API](https://gitlab.com/senac-projetos-de-desenvolvimento/2024-thor-lima-galli/kawabe-api),[Front](https://gitlab.com/senac-projetos-de-desenvolvimento/2024-thor-lima-galli/kawabe-front) [Figma](https://www.figma.com/file/H8tJABD9J1223Sg5pshE2k/Kawabe?type=design&node-id=1%3A28&mode=dev&t=3nuzNz2wo7fQy64V-1)

## Wiki
https://gitlab.com/senac-projetos-de-desenvolvimento/2024-thor-lima-galli/kawabe-docs/-/wikis/home

## Discente:
- Thor Lima Galli
- gallithor@gmail.com


